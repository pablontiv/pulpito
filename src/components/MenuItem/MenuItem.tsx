import React, { useState } from 'react'
import { MenuItemType } from '../../types';
import Menu from '../Menu/Menu';
import styles from './MenuItem.module.scss'

const MenuItem: React.FunctionComponent<MenuItemType> = ({ name, children }) => {
    const [collapsed, setCollapsed] = useState(true);

    const submenuClass = `${styles.submenu} ${(collapsed ?styles.collapsed :styles.expanded)}`

    return (
        <li onMouseEnter={() => setCollapsed(false)} onMouseLeave={() => setCollapsed(true)}>
            <a>{name}</a>
            {(children.length > 0 && !collapsed)}
                <Menu className={submenuClass} menuItems={children}/>
        </li>
    )
}

export default MenuItem
