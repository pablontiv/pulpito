import { MenuType } from '../../types'
import MenuItem from '../MenuItem/MenuItem';
import styles from './Menu.module.scss';

const Menu: React.FunctionComponent<MenuType> = ({className, menuItems }) => {
    return (
        <ul className={`${styles['menu-list']} ${className}`}>
            {menuItems.map(item => <MenuItem key={item.id} id={item.id} name={item.name} children={item.children} />)}
        </ul>
    )
}

export default Menu
