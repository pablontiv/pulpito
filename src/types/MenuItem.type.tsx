export type MenuItemType = {
    id: number
    name: string
    children: []
}
