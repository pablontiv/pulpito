import styles from './App.module.scss'
import Button from "./components/Button/Button"
import Menu from './components/Menu/Menu'

const menuItems = [
  {
    id: 1,
    name: "Menu 1",
    children: [
      {
        id: 11,
        name: "children 1",
        children: [
          {
            id: 111,
            name: "children 1",
            children: []
          },
          {
            id: 112,
            name: "children 1",
            children: []
          },
          {
            id: 113,
            name: "children 1",
            children: []
          }
    
        ]
      },
      {
        id: 12,
        name: "children 1",
        children: []
      },
      {
        id: 13,
        name: "children 1",
        children: []
      }
    ]
  },
  {
    id: 2,
    name: "Menu 2",
    children: []
  },
  {
    id: 3,
    name: "Menu 3",
    children: []
  },
  {
    id: 4,
    name: "Menu 4",
    children: []
  },
  {
    id: 5,
    name: "Menu 5",
    children: []
  },
]

const App: React.FunctionComponent = () => {
  return (
    <div className="App">
      <div className={styles.columns}>
        <div className={styles.sidebar}>
          <Menu menuItems={menuItems}/>
        </div>
        <div className={styles.content}>
          <Button text="hola"></Button>
        </div>
      </div>
    </div>
  );
}

export default App;
